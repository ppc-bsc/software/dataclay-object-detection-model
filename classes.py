import threading
import datetime
import traceback
from collections import OrderedDict
from dataclay import DataClayObject, dclayMethod
from dataclay.contrib.kafka import KafkaMixin
from dataclay.contrib.mqtt import MQTTMixin
"""
City Knowledge Base: collection of Events Snapshots
"""


class DKB(DataClayObject):
    """
    @ClassField kb dict<int, CityNS.classes.EventsSnapshot>
    @ClassField K int
    @ClassField frame_number int
    @ClassField connectedCars list<str>
    @ClassField objects dict<str, CityNS.classes.Object>
    @ClassField original_timestamp int
    @ClassField federated_objects set<str>
    @ClassField federate_compressed_info dict<int, CityNS.classes.FederationCompressedInfo>
    @ClassField traffic_lights dict<<float,float>, CityNS.classes.TrafficLight>
    @ClassField kb_from_adas dict<int, CityNS.classes.EventsSnapshot>
    @ClassField tram_in_station bool

    @dclayImportFrom geolib import geohash
    @dclayImportFrom collections import OrderedDict
    @dclayImportFrom dataclay.api import get_backend_id_by_name
    @dclayImport threading
    @dclayImport traceback
    @dclayImport datetime
    """
    # TODO: add new structure for data coming from the adas, and add method to retrieve this data
    # TODO: add new structure for storing alerts, probably in EventSnapshot -> list of Alerts or similar
    # kb should be a dict<int, CityNS.classes.EventsSnapshot> so snapshots are ordered by timestamp
    # original_timestamp is used in videos only, not in real-time streams

    @dclayMethod(k='int')
    def __init__(self, k=10):
        self.kb = dict()
        self.K = k
        self.frame_number = -1
        self.connectedCars = ["20", "21", "30", "31", "32", "40"]
        self.objects = dict()
        self.global_lock = threading.Lock()
        self.original_timestamp = 0
        self.federated_objects = set()
        self.federate_compressed_info = dict()
        self.traffic_lights = dict()
        self.kb_from_adas = dict()
        self.tram_in_station = False
        # self.backend_id = get_backend_id_by_name("DS1")

    @dclayMethod(eventsSt='list<CityNS.classes.EventsSnapshot>')
    def aggregate_events(self, events_st):
        for event in events_st:
            self.add_events_snapshot(event)

    @dclayMethod()
    def tram_arriving(self):
        self.tram_in_station = True
    
    @dclayMethod()
    def tram_departing(self):
        self.tram_in_station = False

    @dclayMethod()
    def reset_dkb(self):
        self.kb = dict()

    @dclayMethod(k='int')
    def set_k(self, k):
        self.K = k

    @dclayMethod(obj='CityNS.classes.Object')
    def add_object(self, obj):
        obj_id = obj.id_object
        if obj_id not in self.objects:
            self.objects[obj_id] = obj
        
    @dclayMethod(event_snp='CityNS.classes.EventsSnapshot')
    def add_events_snapshot(self, event_snp):
        self.kb[event_snp.timestamp] = event_snp
        self.frame_number += 1

    @dclayMethod(objects_to_store='list<anything>')
    def store_bunk_objects(self, objects_to_store):
        from dataclay import getRuntime
        for object_id, tpx, tpy, tpt, timestamp_last_tp_comp, frame_tp in objects_to_store:
            self.objects[object_id].add_prediction(tpx, tpy, tpt, timestamp_last_tp_comp, frame_tp)
        """
        for retrieval_id, tpx, tpy, tpt, timestamp_last_tp_comp, frame_tp in objects_to_store:
            obj_id, class_id = retrieval_id.split(":")
            obj = getRuntime().get_object_by_id(obj_id, hint=self.backend_id, class_id=class_id)
            obj.add_prediction(tpx, tpy, tpt, timestamp_last_tp_comp, frame_tp)
        """


    # returns a set of all relevant objects from K latest snapshots inside the specified geohashes, based on:
    # If geohashes set is None or empty, then return all relevant objects from latest K snapshots.
    # If with_neighbors == True, return also neighbors; if == False, then return only geohashes specified in set.
    # If with_tp == True, return only objects with trajectory prediction. If == False, then return only objects without
    #           trajectory prediction. If == None, then return all objects (with/without tp).
    # If connected == True, return connected car objects only. If == False then return all non-connected cars.
    # If == None, then return all objects (connected and non-connected).
    @dclayMethod(geohashes='set<str>', with_neighbors='bool', with_tp='bool', connected='bool',
                 events_length_max='dict<str,int>', events_length_min='dict<str,int>', num_objects='int',
                 return_="list<anything>")
    def get_objects(self, geohashes=None, with_neighbors=None, with_tp=None, connected=None, events_length_max=None,
                    events_length_min=None, num_objects=None):
        if events_length_max is None:
            events_length_max = {
                "person": 50,
                "20": 40,
                "30": 40,
                "31": 40,
                "40": 40,
                "car": 40,
                "truck": 40,
                "bus": 40,
                "motor": 40,
                "bike": 40,
                "rider": 50,
                "train": 40
            }
        if events_length_min is None:
            events_length_min = {
                "person": 20,
                "20": 10,
                "30": 10,
                "31": 10,
                "40": 10,
                "car": 10,
                "truck": 10,
                "bus": 10,
                "motor": 10,
                "bike": 10,
                "rider": 20,
                "train": 10
            }
        if geohashes is None:
            geohashes = []
        objs = []
        obj_refs = []
        # for i, (_, event_snap) in enumerate(reversed(OrderedDict(sorted(self.kb.items()))).items()): # get latest updates for objects
        for i, event_snap in enumerate(
                reversed(OrderedDict(sorted(self.kb.items())).values())):  # get latest updates for objects
            if i > self.K or num_objects is not None and len(objs) == num_objects:
                break
            for event in event_snap.events:
                if num_objects is not None and len(objs) == num_objects:
                    break
                obj = event.detected_object
                retrieval_id = obj.retrieval_id  # retrieval_id instead of id_object
                if retrieval_id == "":
                    print("RETRIVAL ID IS EMPTY")
                    continue
                if retrieval_id not in obj_refs:
                    obj_refs.append(retrieval_id)
                    geohash = event.geohash
                    if geohashes is None or len(geohashes) == 0 or geohash in geohashes or with_neighbors is None \
                            or with_neighbors and geohash in [el for n in geohashes for el in geohash.neighbours(n)]:
                        trajectory_px = obj.trajectory_px
                        if with_tp is None or not with_tp and len(trajectory_px) == 0 \
                                or with_tp and len(trajectory_px) > 0 and trajectory_px[0] != 0:
                            obj_type = obj.type
                            if connected is None or not connected and obj_type not in self.connectedCars \
                                    or connected and obj_type in self.connectedCars:
                                # objs.append((id_object, trajectory_px, obj.trajectory_py, obj.trajectory_pt, geohash,
                                #     [list(ev.convert_to_dict().values()) for ev in list
                                #     (OrderedDict(sorted(obj.events_history.items())).values())]))
                                # objs.append((id_object, trajectory_px, obj.trajectory_py, obj.trajectory_pt, geohash,
                                #     obj.get_events_history()))
                                obj_type = obj.type
                                dequeues = obj.get_events_history(events_length_max[obj_type])
                                timestamp = dequeues[2][-1]  # timestamp dequeue and [-1] to get ts of last event
                                if len(dequeues[0]) >= events_length_min[obj_type]:
                                    if with_tp is None or not with_tp:  # and
                                        if dequeues[2][-1] > obj.timestamp_last_tp_comp:
                                            # or with_tp:  # condition of > is only active for the TP invocation
                                            objs.append((retrieval_id, trajectory_px, obj.trajectory_py,
                                                         obj.trajectory_pt,
                                                         geohash, dequeues, obj.id_object,
                                                         int(event_snap.snap_alias.split("_")[1]), event.pixel_w,
                                                         event.pixel_h))
                                    else:  # for CD
                                        print(f"Trajectory predicition has {len(trajectory_px)} which are {trajectory_px}")
                                        objs.append((trajectory_px, obj.trajectory_py, obj.trajectory_pt, geohash,
                                                     obj.id_object, obj.type, obj.frame_last_tp))

        return objs

    @dclayMethod(object_id='str', object_type='str', return_='CityNS.classes.Object')
    def get_or_create(self, object_id, object_type):
        if not hasattr(self, "global_lock") or self.global_lock is not None:
            self.global_lock = threading.Lock()
        with self.global_lock:
            if object_id not in self.objects:
                obj = Object(object_id, object_type)
                self.objects[object_id] = obj
            else:
                obj = self.objects[object_id]
        return obj

    """ TODO: objects coming from {get,set}state are not linked to DKB
    @dclayMethod(return_="dict<str, anything>")
    def __getstate__(self):
        # if KB is serialized, objects that are not persistent will be automatically persisted
        return {"kb": self.kb, "K": self.K, "frame_number": self.frame_number,
                "connectedCars": self.connectedCars, "objects": self.objects,
                "original_timestamp": self.original_timestamp}

    @dclayMethod(state="dict<str, anything>")
    def __setstate__(self, state):
        self.kb = state["kb"]
        self.K = state["K"]
        self.frame_number = state["frame_number"]
        self.connectedCars = state["connectedCars"]
        self.objects = state["objects"]
        self.original_timestamp = state["original_timestamp"]
        self.global_lock = threading.Lock()
    """

    @dclayMethod(timestamp='int', unfederate_objects='bool')
    def remove_old_snapshots_and_objects(self, timestamp: int, unfederate_objects: bool):
        if not hasattr(self, "global_lock") or self.global_lock is not None:
            self.global_lock = threading.Lock()
        for snap_timestamp in list(self.kb.keys()):
            if snap_timestamp < timestamp:
                snapshot = self.kb[snap_timestamp]
                if snapshot.get_replica_locations() is not None and len(snapshot.get_replica_locations()) > 0:
                    print(
                        f"Deleting snapshot at timestamp {snap_timestamp} and snap_alias {snapshot.snap_alias}")
                    snapshot.delete(unfederate_objects)
                    del self.kb[snap_timestamp]
                elif snap_timestamp in self.federate_compressed_info:
                    print(
                        f"Deleting snapshot at timestamp {snap_timestamp} and snap_alias {snapshot.snap_alias}")
                    snapshot.delete(unfederate_objects)
                    del self.kb[snap_timestamp]
                    if unfederate_objects:
                        compressed_info = self.federate_compressed_info[snap_timestamp]
                        compressed_info.unfederate()
                    del self.federate_compressed_info[snap_timestamp]

        for object_id in list(self.objects.keys()):
            obj = self.objects[object_id]
            if obj.timestamp < timestamp:
                if obj.get_replica_locations() is not None and len(obj.get_replica_locations()) > 0:
                    with self.global_lock:
                        print(f"Deleting object id {object_id}")
                        obj.delete(unfederate_objects)
                        del self.objects[object_id]
                elif object_id in self.federated_objects:
                    with self.global_lock:
                        print(f"Deleting object id {object_id}")
                        obj.delete(False)
                        del self.objects[object_id]
                    self.federated_objects.remove(object_id)


    @dclayMethod(snapshot='CityNS.classes.EventsSnapshot', backend_id='anything')
    def federate_compressed_snapshot(self, snapshot, backend_id):
        compressed_info = FederationCompressedInfo(snapshot, self)
        compressed_info.federate_to_backend(backend_id)
        # indicates compressed objects send after federation
        # RACE CONDITION: two threads federate same object O, but there is get_or_create at the cloud!
        for obj_id in compressed_info.objects.keys():
            self.federated_objects.add(obj_id)
        self.federate_compressed_info[snapshot.timestamp] = compressed_info


    @dclayMethod(json_msg='dict<str, anything>', return_='CityNS.classes.TrafficLight')
    def update_traffic_light_status(self, json_msg):
        """Creates or updates a traffic light object given a JSON

        :param json_msg: json to parse
        :return: The TrafficLight that has been created or updated
        """
        # Check traffic light object already exists or not
        longitude = float(json_msg["CoordX"])
        latitude = float(json_msg["CoordY"])

        print(f"Update traffic light in progress. Relevant attributes:\n"
              f"    id={ json_msg['TrafficLightId'] }\n"
              f"    coordinates=({ longitude }, { latitude })")

        if (longitude, latitude) not in self.traffic_lights:
            print("TrafficLight instance is being created (it was not found in traffic_lights dictionary)")
            
            tl = TrafficLight(json_msg["TrafficLightId"], longitude, latitude, json_msg["CurrentStatus"],
                              json_msg["TrafficLightType"], json_msg["TrafficLightGroup"], json_msg["TLRMId"],
                              datetime.datetime.fromisoformat(json_msg["Timestamp"]))
            self.traffic_lights[longitude, latitude] = tl
        else:
            tl = self.traffic_lights[longitude, latitude]
            tl.status = json_msg["CurrentStatus"]
            tl.timestamp = datetime.datetime.fromisoformat(json_msg["Timestamp"])

        return tl

    @dclayMethod(json_msgs='anything')
    def add_events_from_json_adas(self, json_msgs):
        for timestamp in json_msgs:
            event_snap_adas = EventsSnapshot(alias=f"adas_{timestamp}")
            for event_data in json_msgs[timestamp]:
                event = Event()
                event.init_from_detected_object_json(event_data, self)
                event_snap_adas.add_event(event)
                # self.add_event_from_adas_to_snapshot(event)
                # TODO: where do we append the events coming from the adas? To the DKB directly or in a new structure?
                # To a snapshot based on timestamp? Passing dkb (self) to init_from_detected_object_json(...) so it
                # can be added from there?
            self.kb_from_adas[timestamp] = event_snap_adas

    @dclayMethod(event="CityNS.classes.Event")
    def add_event_from_adas_to_snapshot(self, event):
        min_difference = event.timestamp  # next(iter(self.kb.keys()))
        event_timestamp = event.timestamp
        selected_snap = None
        for snap_timestamp, snapshot in reversed(OrderedDict(sorted(self.kb.items())).items()):
            if min_difference > abs(event_timestamp - snap_timestamp):
                min_difference = abs(event_timestamp - snap_timestamp)
                selected_snap = snapshot

        # if selected_snap is None there are no snapshots yet in the system, we can ignore data received from tram ???
        if selected_snap is not None:
            selected_snap.add_event(event)


    @dclayMethod(category='str', return_='int')
    def convert_type_to_int(self, category):
        ret = 0
        if category == 'car':
            ret = 1
        elif category == 'truck':
            ret = 2
        elif category == 'bus':
            ret = 3
        elif category == 'motor':
            ret = 4
        elif category == 'bike':
            ret = 5
        elif category == 'rider':
            ret = 6
        elif category == 'traffic light':
            ret = 7
        elif category == 'traffic sign':
            ret = 8
        elif category == 'train':
            ret = 9
        return ret


    """
    Either return latest information coming from ADAS so it can be used in the deduplicator (CURRENT APPROACH),
    or pass a timestamp as argument and check which snapshot in the DKB is the closest to the timestamp received
    """
    @dclayMethod(return_='anything')
    def get_latest_info_from_adas(self):
        source_id = 99999  # Hardcoded for tram sources
        events_to_return = []
        snap_timestamp, snapshot = list(reversed(OrderedDict(sorted(self.kb_from_adas.items())).items()))[0]
        for event in snapshot:
            events_to_return.append((event.latitude_pos, event.longitude_pos,
                                     convert_type_to_int(event.detected_object.type),
                                     event.speed, event.yaw, event.detected_object.id_object.split("_")[1],
                                     source_id))
            # TODO: type (str) is not the same as class in deduplicator (int)
            # TODO: append cam_id to events_to_return as an integer (99999 f.e.) -> source_id
            # TODO: object_id should be just tracked integer, not 20939_1 -> id_object.split("_")[1]
            # No pixel data nor frame as it is coming from tram/adas

        return events_to_return


"""
Events Snapshots: List of the events detected in a snapshot. Each event
points to detected object.
"""
class EventsSnapshot(DataClayObject):
    """
    @ClassField events list<CityNS.classes.Event>
    @ClassField snap_alias str
    @ClassField timestamp int
    @ClassField kafka_topic str
    @ClassField print_federated_events bool
    @ClassField trigger_modena_tp bool
    @ClassField alerts list<CityNS.classes.Alert>

    @dclayImportFrom geolib import geohash
    @dclayImport pygeohash as pgh
    @dclayImport requests
    @dclayImport traceback
    """

    @dclayMethod(alias='str', kafka_topic='str', print_federated_events='bool', trigger_modena_tp='bool')
    def __init__(self, alias, kafka_topic=None, print_federated_events=False, trigger_modena_tp=False):
        self.events = list()
        self.snap_alias = alias
        # self.session = requests.Session()
        self.timestamp = 0
        self.kafka_topic = kafka_topic
        self.print_federated_events = print_federated_events
        self.trigger_modena_tp = trigger_modena_tp
        self.alerts = list()

    @dclayMethod(event='CityNS.classes.Event')
    def add_event(self, event):
        self.events.append(event)

    @dclayMethod(alert='CityNS.classes.Alert')
    def add_alert(self, alert):
        self.alerts.append(alert)

    @dclayMethod(return_='list<CityNS.classes.Alert>')
    def get_alerts(self):
        # TODO: probably better returning only info from alerts, not Alerts itself -> similar to get_objects
        return self.alerts

    @dclayMethod(events_detected='anything', kb='CityNS.classes.DKB')
    def add_events_from_trackers(self, events_detected, kb):
        classes = ["person", "car", "truck", "bus", "motor", "bike", "rider", "traffic light", "traffic sign", "train"]
        self.timestamp = events_detected[0]  # TODO: replace for real-streams
        """  # TODO: only for videos
        if self.snap_alias.split("_")[1] == "0":  # frame 0
            self.timestamp = events_detected[0]
            kb.original_timestamp = self.timestamp
        else:
            self.timestamp = kb.original_timestamp + int(self.snap_alias.split("_")[1]) * 1000 // 24
        # self.timestamp = 1611592497727 + int(self.snap_alias.split("_")[1])*400 # to debug purpose only
        """
        for index, ev in enumerate(events_detected[1]):
            id_cam = ev[0]
            if id_cam == 0 or id_cam == "0":  # TODO: to be removed, debug only
                print("CAM ID RECEIVED NOT CORRECT")
            tracker_id = ev[1]
            tracker_class = ev[2]
            vel_pred = ev[3]
            yaw_pred = ev[4]
            lat = ev[5]
            lon = ev[6]
            x = ev[7]
            y = ev[8]
            w = ev[9]
            h = ev[10]
            frame = ev[11]
            id_object = str(id_cam) + "_" + str(tracker_id)
            if tracker_class in [20, 21, 30, 31, 32, 40]:
                obj = kb.get_or_create(id_object, str(tracker_class))
            else:
                obj = kb.get_or_create(id_object, classes[tracker_class])
            event = Event()
            geohash = pgh.encode(lat, lon, precision=7)
            event.initialize(obj, self.timestamp, vel_pred, yaw_pred, float(lon), float(lat), x, y, w, h, geohash, frame)
            obj.add_event(event)
            self.events.append(event)

        kb.add_events_snapshot(self)  # persists snapshot

    """
    @dclayMethod()
    def when_unfederated(self):
        # TODO
        #kb = DKB.get_by_alias("DKB")
        #kb.remove_events_snapshot(self)
    """

    @dclayMethod(return_='str')
    def __repr__(self):
        return f"Events Snapshot: \n\tevents: {self.events}, \n\tsnap_alias: " \
               f"{self.snap_alias}, timestamp: {self.timestamp}"


    @dclayMethod()
    def when_federated(self):
        try:
            kb = DKB.get_by_alias("DKB")
            kb.add_events_snapshot(self)
            for event in self.events:
                obj = event.detected_object
                kb.add_object(obj)
                #### Associate federated event to detected object in cloud
                ## events are federated and pointing to object already present in cloud E -> O but is not O -> E
                obj.add_event(event)
                object_id = obj.get_object_id()
                class_id = obj.get_class_extradata().class_id
                obj.retrieval_id = str(object_id) + ":" + str(class_id)

            self.trigger_after_federation()

        except Exception:
            traceback.print_exc()

    @dclayMethod()
    def trigger_after_federation(self):
        if self.print_federated_events:
            self.echo_federated_events()
        if self.kafka_topic is not None and self.kafka_topic.strip():
            self.send_events_to_kafka()
        if self.trigger_modena_tp:
            self.trigger_trajectory_prediction()

    @dclayMethod(unfederate_objects='bool')
    def delete(self, unfederate_objects):
        if unfederate_objects:
            self.unfederate(recursive=False)  # implies an unfederate of the events and the objects of those events
        self.session_detach()
        # clean events
        self.events = list()

    @dclayMethod()
    def echo_federated_events(self):
        for event in self.events:
            detected_object = event.detected_object
            detected_object_id = detected_object.id_object
            id_cam = detected_object_id.split("_")[0]
            # iteration = self.snap_alias.split("_")[1]  # aka frame
            iteration = event.frame  # aka frame
            ts = self.timestamp
            lat = event.latitude_pos
            lon = event.longitude_pos
            speed = event.speed
            yaw = event.yaw
            geohash = event.geohash
            if detected_object.frame_last_tp != -1:
                print(
                    f"""WF LOG FILE: {id_cam} {iteration} {ts} {detected_object.type} {lat} {lon} """
                    f"""{geohash} {speed} {yaw} {detected_object_id} {event.pixel_x} """
                    f"""{event.pixel_y} {event.pixel_w} {event.pixel_h} """
                    f"""{detected_object.frame_last_tp} {next(reversed(sorted(detected_object.events_history)))} """
                    f"""{detected_object.trajectory_px} {detected_object.trajectory_py} """
                    f"""{detected_object.trajectory_pt}""")
            else:
                print(
                    f"""WF LOG FILE: {id_cam} {iteration} {ts} {detected_object.type} {lat} {lon} """
                    f"""{geohash} {speed} {yaw} {detected_object_id} {event.pixel_x} """
                    f"""{event.pixel_y} {event.pixel_w} {event.pixel_h} """
                    f"""{detected_object.frame_last_tp} {-1} 0,0,0,0,0 0,0,0,0,0 0,0,0,0,0""")

    @dclayMethod()
    def send_events_to_kafka(self):
        for event in self.events:
            # TODO: If this is used, then the station should be properly set
            event.send_to_kafka(self.kafka_topic, "undefined")

    @dclayMethod()
    def trigger_trajectory_prediction(self):
        # WARNING: only working for Modena cloud
        try:
            # trigger prediction via REST with alias specified for last EventsSnapshot
            apihost = 'https://192.168.7.42:31001'  # 'https://192.168.7.40:31001'
            auth_key = \
                '23bc46b1-71f6-4ed5-8c54-816aa4f8c502:123zO3xZCLrMN6v2BKK1dXYFpXlPkccOFqm12CdAsMgRU4VrNZ9lyGVCGuMDGIwP'
            namespace = '_'
            blocking = 'false'
            result = 'false'
            # trigger = 'tp-trigger'
            # url = apihost + '/api/v1/namespaces/' + namespace + '/triggers/' + trigger
            tp_action = 'tpAction'
            cd_action = 'cdAction'
            url = apihost + '/api/v1/namespaces/' + namespace + '/actions/' + tp_action + \
                  '?blocking=false&result=false'  # asynchronous invocation
            url_cd = apihost + '/api/v1/namespaces/' + namespace + '/actions/' + cd_action + \
                     '?blocking=false&result=false'  # asynchronous invocation
            user_pass = auth_key.split(':')
            # alias = snap_alias
            alias = "DKB"
            session = requests.Session()
            session.post(url, params={'blocking': blocking, 'result': result},
                                  json={"ALIAS": str(alias)}, auth=(user_pass[0], user_pass[1]),
                                  verify=False)  # keep alive the connection
            session.post(url_cd, params={'blocking': blocking, 'result': result},
                                  json={"ALIAS": str(alias)}, auth=(user_pass[0], user_pass[1]),
                                  verify=False)  # keep alive the connection
        except Exception:
            traceback.print_exc()
            print("Error in REST API connection in when_federated.")

"""
Object: Vehicle or Pedestrian detected
        Objects are classified by type: Pedestrian, Bicycle, Car, Track, ...
        Only if it is not a pedestrian, the values speed and yaw are set.
"""


class Object(DataClayObject, KafkaMixin):
    """
    @ClassField id_object str
    @ClassField type str
    @ClassField events_history dict<int, CityNS.classes.Event>
    @ClassField timestamp int
    @ClassField trajectory_px list<float>
    @ClassField trajectory_py list<float>
    @ClassField trajectory_pt list<int>
    @ClassField retrieval_id str
    @ClassField timestamp_last_tp_comp int
    @ClassField frame_last_tp int
    """

    @dclayMethod(id_object='str', obj_type='str')
    def __init__(self, id_object, obj_type):
        self.id_object = id_object
        self.type = obj_type
        self.events_history = dict()
        self.timestamp = 0
        self.trajectory_px = []
        self.trajectory_py = []
        self.trajectory_pt = []
        self.retrieval_id = ""  # added for retrieval purposes for IBM
        self.timestamp_last_tp_comp = -1
        self.frame_last_tp = -1


    @dclayMethod(event='CityNS.classes.Event')
    def add_event(self, event):
        # TODO: two events at same timestamp? (data coming from ADAS and NGAP?)
        self.events_history[event.timestamp] = event
        if self.timestamp < event.timestamp:
            self.timestamp = event.timestamp

    # Updates the trajectory prediction
    @dclayMethod(tpx='list<anything>', tpy='list<anything>', tpt='list<anything>', timestamp_last_tp_comp='int',
                 frame_tp='int')
    def add_prediction(self, tpx, tpy, tpt, timestamp_last_tp_comp, frame_tp):
        # import numpy
        self.trajectory_px = tpx
        self.trajectory_py = tpy
        self.trajectory_pt = tpt
        self.timestamp_last_tp_comp = timestamp_last_tp_comp
        obj_hist_ts = next(reversed(sorted(self.events_history)))
        self.frame_last_tp = frame_tp
        print(f"""TP LOG FILE: {frame_tp} {obj_hist_ts} {self.id_object} {self.trajectory_px} {self.trajectory_py} """
              f"""{self.trajectory_pt}""")

    # Returns the Object and its Events history (Deque format)
    @dclayMethod(events_length_max='int', return_="anything")
    def get_events_history(self, events_length_max):
        from collections import deque, OrderedDict
        # Events in following format(dqx, dqy, dqt)
        dqx = deque()
        dqy = deque()
        dqt = deque()
        i = 0
        for _, event in reversed(OrderedDict(sorted(self.events_history.items())).items()):
            if i == events_length_max:
                break
            dqx.insert(0, event.latitude_pos)
            dqy.insert(0, event.longitude_pos)
            dqt.insert(0, event.timestamp)  # dequeues should be ordered with increasingly timestamps
            i += 1
        return dqx, dqy, dqt

    @dclayMethod(return_='str')
    def __repr__(self):
        num_events = len(self.events_history)
        return f"Object {self.id_object} of type {self.type} with {num_events} events"  # Events {self.events_history}"


    @dclayMethod(return_='dict<str,anything>')
    def get_compressed_data(self):
        return {"id_object": self.id_object, "type": self.type, "trajectory_px": self.trajectory_px,
                "trajectory_py": self.trajectory_py, "trajectory_pt": self.trajectory_pt, 
                "timestamp": self.timestamp}

    @dclayMethod(unfederated_object='bool')
    def delete(self, unfederate_object):
        for event in list(self.events_history.values()):
            event.delete()
        if unfederate_object:
            self.unfederate()
        self.events_history.clear()
        self.session_detach()


"""
Event: Instantiation of an Object for a given position and time.
"""


class Event(DataClayObject, KafkaMixin):
    """
    @ClassField id_event int
    @ClassField detected_object CityNS.classes.Object
    @ClassField timestamp int
    @ClassField speed anything
    @ClassField yaw anything
    @ClassField longitude_pos anything
    @ClassField latitude_pos anything
    @ClassField pixel_x int
    @ClassField pixel_y int
    @ClassField pixel_w int
    @ClassField pixel_h int
    @ClassField geohash str
    @ClassField frame int

    @dclayImport traceback
    @dclayImport datetime
    """

    @dclayMethod()
    def __init__(self):
        import uuid
        self.id_event = uuid.uuid4().int
        self.detected_object = None
        self.timestamp = 0
        self.speed = 0
        self.yaw = 0
        self.longitude_pos = 0.0
        self.latitude_pos = 0.0
        self.pixel_x = 0
        self.pixel_y = 0
        self.pixel_w = 0
        self.pixel_h = 0
        self.geohash = ""
        self.frame = -1

    @dclayMethod(detected_object='CityNS.classes.Object', timestamp='int', speed='anything',
                 yaw='anything', longitude_pos='anything', latitude_pos='anything', x='int',
                 y='int', w='int', h='int', geohash='str', frame='int')
    def initialize(self, detected_object, timestamp, speed, yaw, longitude_pos, latitude_pos, 
                   x, y, w, h, geohash, frame):
        self.detected_object = detected_object
        self.timestamp = timestamp
        self.speed = speed
        self.yaw = yaw
        self.longitude_pos = longitude_pos
        self.latitude_pos = latitude_pos
        self.pixel_x = x
        self.pixel_y = y
        self.pixel_w = w
        self.pixel_h = h
        self.geohash = geohash
        self.frame = frame

    @dclayMethod(fiware_json='dict<str, anything>', kb='CityNS.classes.DKB')
    def init_from_fiware_vehicle(self, fiware_json, kb):
        """
        Initialize event given a FIWARE json representing Vehicle tram: creates one event representing
        the position of the tram.
        :param json_msg: FIWARE json to parse
        :param kb: DKB
        :return: None
        """
        # Check tram object already exists or not, to add the event
        self.detected_object = kb.get_or_create(fiware_json["id"], "tram")
        self.detected_object.add_event(self)
        #TODO: self.timestamp = fiware_json[""]
        self.speed = int(fiware_json["speed"])
        self.yaw = float(fiware_json["heading"])
        self.longitude_pos = float(fiware_json["location"]["coordinates"][0])
        self.latitude_pos = float(fiware_json["location"]["coordinates"][1])
        #TODO: geohash moved to Event, needs to be regenerated based on (lat, lon)
        #TODO: add event to KB? which snapshot? currently object is added to DKB with the event
        #TODO: distributed design: is get_or_create always executed at the same backend?

    @dclayMethod(json_msg='dict<str, anything>', kb='CityNS.classes.DKB')
    def init_from_detected_object_json(self, json_msg, kb):
        """
        Initialize event given a JSON representing a detected object: creates one event representing
        a detected object by the tram.
        :param json_msg: json to parse
        :param kb: DKB
        :return: None
        """
        # Check tram object already exists or not, to add the event
        self.detected_object = kb.get_or_create(f"tram_{json_msg['id_object']}", json_msg["type"])  # TODO: since this
        # method is called before the deduplicator, we will have duplicated objects!!!!!!!!
        self.detected_object.add_event(self)
        self.speed = int(json_msg["speed"])
        self.yaw = float(json_msg["heading"])
        # TODO: transform lat, lon based on received tram lat, lon (json_msg["longitude/latitude"])
        self.longitude_pos = float(json_msg["longitude"])
        self.latitude_pos = float(json_msg["latitude"])
        self.timestamp = int(datetime.datetime.fromisoformat(f"{json_msg['timestamp'].strip('Z')}+00:00").timestamp()*1000)
        print(f"Event received from ADAS regarding object {self.detected_object.id_object} of type "
              f"{self.detected_object.type} with speed {self.speed} and heading {self.yaw} at latitude "
              f"{self.latitude_pos} and longitude {self.longitude_pos} at timestamp {self.timestamp}, and "
              f"positionX={json_msg['object_positionX_m']}, positionY={json_msg['object_positionY_m']}, "
              f"positionZ={json_msg['object_positionZ_m']}")
        #TODO: geohash moved to Event    ---->   self.geohash = str(json_msg["geohash"])
        #TODO: add event to KB? which snapshot? currently object is added to DKB with the event
        # GeoHash Batoni: spzcpk
        # GeoHash Arcipresi: spzbzp
        # GeoHash Resistenza: spzbym

    @dclayMethod(kafka_topic='str', station='str')
    def send_to_kafka(self, kafka_topic, station):
        event_values = {}
        event_values["id_object"] = self.detected_object.id_object
        event_values["type"] = self.detected_object.type
        event_values["speed"] = self.speed
        event_values["longitude"] = self.longitude_pos
        event_values["latitude"] = self.latitude_pos
        event_values["timestamp"] = self.timestamp
        event_values["geohash"] = self.geohash
        event_values["station"] = station
        self.produce_kafka_msg(event_values, kafka_topic)


    @dclayMethod(kb='CityNS.classes.DKB')
    def on_arrival_from_mqtt_bridge(self, kb):
        """
        For events arriving from mqtt bridge, either via when_federated or directly from the bridge,
        trigger current functionality
        """
        #TODO: add event to KB? which snapshot? currently object is added to DKB with the event
        pass

    @dclayMethod()
    def when_federated(self):
        # Add federated event object to detected objects of DKB
        try:
            kb = DKB.get_by_alias("DKB")
            obj = self.detected_object
            kb.add_object(obj)
            #### Associate federated event to detected object in cloud
            ## events are federated and pointing to object already present in cloud E -> O but is not O -> E
            obj.add_event(self)
            self.on_arrival_from_mqtt_bridge(kb)
        except Exception:
            traceback.print_exc()

    @dclayMethod(return_='str')
    def __repr__(self):
        return "(long=%s,lat=%s,time=%s,speed=%s,yaw=%s,id=%s,id_object=%s,geohash=%s,frame=%s)" % (
            str(self.longitude_pos), str(self.latitude_pos),
            str(self.timestamp), str(self.speed), str(self.yaw),
            str(self.id_event), str(self.detected_object.id_object), str(self.geohash), str(self.frame))

    @dclayMethod(return_='dict<str, anything>')
    def get_compressed_data(self):
        return {"id_event": self.id_event, "detected_object": self.detected_object.id_object,
                "timestamp": self.timestamp, "speed": self.speed, "yaw": self.yaw, "latitude_pos": self.latitude_pos,
                "longitude_pos": self.longitude_pos, "x": self.pixel_x, "y": self.pixel_y, "w": self.pixel_w, 
                "h": self.pixel_h, "geohash": self.geohash, "frame": self.frame}

    @dclayMethod()
    def delete(self):
        # self.detected_object = None
        self.session_detach()

class Alert(DataClayObject, MQTTMixin, KafkaMixin):
    """
    @ClassField id str
    @ClassField alert_source str
    @ClassField subcategory str
    @ClassField severity str
    @ClassField longitude float
    @ClassField latitude float
    @ClassField timestamp anything
    @ClassField valid_from anything
    @ClassField valid_to anything
    @ClassField area_served str
    @ClassField description str
    @ClassField data anything
    """
    # alert_source is IP address of the device/station where the alert has been generated
    # timestamp, valid_from and valid_to field are datetime in ISO8601 UTC format - https://docs.python.org/3/library/datetime.html

    @dclayMethod(id = "str", source="str", alert_category="str", severity="str", longitude="float", latitude="float",
                 timestamp="anything", valid_from="anything", valid_to="anything", area="str",
                 description="str", data="anything")
    def __init__(self, id, source, alert_category, severity, longitude, latitude, timestamp, valid_from=None, valid_to=None,
                 area=None, description=None, data=None):
        import uuid
        severity_level = ["informational", "low", "medium", "high", "critical"]
        if severity not in severity_level:
            raise RuntimeError("Alert severity '" + severity + "' not valid. Allowed values: informational, low, medium, high, critical")
        subcategory_type = ["trafficJam", "carAccident", "carWrongDirection", "carStopped", "pothole", "roadClosed",
                            "roadWorks", "hazardOnRoad", "injuredBiker", "tramApproaching", "bikerOnRoad",
                            "pedestrianOnRoad"]
        if alert_category not in subcategory_type:
            raise RuntimeError("Alert category '" + alert_category + "' not valid. Allowed values: trafficJam, carAccident, carWrongDirection, carStopped, pothole, roadClosed, roadWorks, hazardOnRoad, injuredBiker, tramApproaching, bikerOnRoad, pedestrianOnRoad")
        # Fiware Mandatory fields
        self.id = id
        # self.type = "Alert"
        self.alert_source = source
        # self.category = "traffic"
        self.subcategory = alert_category
        self.severity = severity
        self.timestamp = timestamp
        # Optional fields useful in ELASTiC
        self.longitude = longitude
        self.latitude = latitude
        self.valid_from = valid_from
        self.valid_to = valid_to
        self.area_served = area
        self.description = description
        self.data = data

    @dclayMethod()
    def __generate_fiware_msg(self):
        return {"id": self.id, "type": "Alert", "alertSource": self.alert_source, "category": "traffic",
                "subCategory": self.subcategory, "severity": self.severity, "dateIssued": self.timestamp.isoformat(),
                "location": { "type": "Point", "coordinates": [self.longitude, self.latitude] },
                "validFrom": self.valid_from.isoformat(), "validTo": self.valid_to.isoformat(),
                "description": self.description, "data": self.data}

    @dclayMethod(topic='str')
    def send_to_mqtt(self, topic="alerts"):
        print("Ready to send %s to MQTT through topic %s" % (self.id, topic))
        self.produce_mqtt_msg(self.__generate_fiware_msg(), topic)

    @dclayMethod(topic='str', station='str')
    def send_to_kafka(self, topic, station):
        print("Ready to send %s to Kafka through topic %s" % (self.id, topic))
        data = self.__generate_fiware_msg()
        data["station"] = station
        self.produce_kafka_msg(data, topic)

class TrafficLight(DataClayObject, KafkaMixin):
    """
    @ClassField id str
    @ClassField longitude float
    @ClassField latitude float
    @ClassField status str
    @ClassField type str
    @ClassField group str
    @ClassField TLRM_id str
    @ClassField timestamp anything
    """

    @dclayMethod(id='str', lon='float', lat='float', status='str', type='str', group='str', TLRM_id='str', timestamp='anything')
    def __init__(self, id, lon, lat, status, type, group, TLRM_id, timestamp):
        colors = ["Green", "Yellow", "Red"]
        if status not in colors:
            raise RuntimeError("Traffic light status '" + status + "' not valid")
        tl_type = ["Pedonale Tram", "Pedonale Veicolare", "Tram", "Veicolare"]
        if type not in tl_type:
            raise RuntimeError("Traffic light type '" + type + "' not valid")

        #Example: 1p01bis
        self.id = id
        #Example: 11.2216092239356
        self.longitude = lon
        #Example: 43.7741609499214
        self.latitude = lat
        #Example: Green
        self.status = status
        #Example: Pedonale Tram
        self.type = type
        #Example: Opposto 1Tram1 (not useful for real-time, just holding it for offline)
        self.group = group
        #Example: BATONI (not useful for real-time, just holding it for offline)
        self.TLRM_id = TLRM_id
        self.timestamp = timestamp

    @dclayMethod(return_='str')
    def __repr__(self):
        return f"Traffic light: [id={self.id}, type={self.type}, longitude={self.longitude}, latitude={self.latitude}, status={self.status}, timestamp={self.timestamp}]"

    @dclayMethod(kafka_topic='str', station='str')
    def send_to_kafka(self, kafka_topic, station):
        tf_values = {}
        tf_values["id"] = self.id
        tf_values["type"] = self.type
        tf_values["longitude"] = self.longitude
        tf_values["latitude"] = self.latitude
        tf_values["status"] = self.status
        tf_values["group"] = self.group
        tf_values["TLRM_id"] = self.TLRM_id
        tf_values["timestamp"] = str(self.timestamp)
        tf_values["station"] = station
        self.produce_kafka_msg(tf_values, kafka_topic)

class FederationCompressedInfo(DataClayObject):
    """
    @ClassField events list<anything>
    @ClassField objects dict<str, anything>
    @ClassField snap_alias str
    @ClassField timestamp int
    @ClassField kafka_topic str
    @ClassField print_federated_events bool
    @ClassField trigger_modena_tp bool

    @dclayImport traceback
    @dclayImport uuid
    @dclayImport traceback
    """

    @dclayMethod(snapshot='CityNS.classes.EventsSnapshot', dkb='CityNS.classes.DKB')
    def __init__(self, snapshot, dkb):
        self.events = []
        self.snap_alias = snapshot.snap_alias
        self.timestamp = snapshot.timestamp
        self.kafka_topic = snapshot.kafka_topic
        self.print_federated_events = snapshot.print_federated_events
        self.trigger_modena_tp = snapshot.trigger_modena_tp
        self.objects = dict()
        for event in snapshot.events:
            self.events.append(event.get_compressed_data())
            obj = event.detected_object
            obj_id = obj.id_object
            if obj_id not in self.objects and obj_id not in list(dkb.federated_objects):
                self.objects[obj_id] = obj.get_compressed_data()
                dkb.federated_objects.add(obj_id)


    @dclayMethod()
    def when_federated(self):
        try:
            kb = DKB.get_by_alias("DKB")
            snapshot = EventsSnapshot(self.snap_alias, kafka_topic=self.kafka_topic,
                                      print_federated_events=self.print_federated_events,
                                        trigger_modena_tp=self.trigger_modena_tp)
            snapshot.timestamp = self.timestamp
            kb.add_events_snapshot(snapshot)
            for compressed_obj_id, compressed_object in self.objects.items():
                obj_class = compressed_object["type"]
                obj = kb.get_or_create(compressed_obj_id, obj_class)
                object_id = obj.get_object_id()
                class_id = obj.get_class_extradata().class_id
                obj.retrieval_id = str(object_id) + ":" + str(class_id)
                # indicates KB that object decompressed was federated (for deleting purposes)
                if object_id not in kb.federated_objects:
                    kb.federated_objects.add(object_id)

            for compressed_event in self.events:
                detected_object_id = compressed_event["detected_object"]
                detected_object = kb.objects[detected_object_id]
                event = Event()
                event.initialize(detected_object, compressed_event["timestamp"],
                                 compressed_event["speed"], compressed_event["yaw"], compressed_event["longitude_pos"],
                                 compressed_event["latitude_pos"], compressed_event["latitude_pos"], 
                                 compressed_event["x"], compressed_event["y"], compressed_event["w"], 
                                 compressed_event["h"], compressed_event["geohash"], compressed_event["frame"])
                event.id_event = compressed_event["id_event"]
                detected_object.add_event(event)
                snapshot.add_event(event)

            snapshot.trigger_after_federation()
            # indicates KB that the snapshot decompressed was federated (for deleting purposes)
            kb.federate_compressed_info[snapshot.timestamp] = self


        except Exception:
            traceback.print_exc()
